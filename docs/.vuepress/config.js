module.exports = {
    title: 'VuePress',
    description: 'Just playing around',
    base: '/docs/',
    dest: 'public/docs',
    head: [
        ['link', { rel: 'icon', href: '/icon.png' }]
    ],
    themeConfig: {
        nav: [
            { text: 'Home', link: '/' },
            { text: 'Guide', link: '/guide/' }
        ],
        sidebar: [
            ['http://laravel-vuepress.test', 'Laravel'],
            ['/', 'Home'],
            ['/guide/', 'Guide'],
        ]
    }
}
