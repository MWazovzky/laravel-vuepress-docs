<?php

namespace App\Http\Controllers;

use File;

class DocsController extends Controller
{
    public function index(string $file = 'index')
    {
        if ($file !== 'index') {
            $file = $file . '/index';
        }

        return File::get(public_path("/docs/{$file}.html"));
    }
}
